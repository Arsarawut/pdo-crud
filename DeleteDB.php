<?php 

require "./connection.php";

$id = 1;

$sql = "delete from users where id = ?";
$stm = $con->prepare($sql);
$stm->bindParam("1", $id);


try {
	$stm->execute();
	echo "ลบข้อมูลเรียบร้อย";
} catch (Exception $e) {
	echo $e->getTraceAsString();
}